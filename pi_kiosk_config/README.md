# Uptwinkles Pi Kiosk

This configuration is what causes the Pi to boot up and display the Uptwinkles dashboard on screen.

1. Install Raspbian on the Raspberry Pi.
2. Unpack this directory, aside from this readme, into the root filesystem.
