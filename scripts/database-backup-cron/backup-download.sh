#!/bin/bash
DATE=`date +%F`
TARFILE=$DATE-wekan-db.tar

# Create a backup on server
echo "Creating remote backup..."
ssh root@uptwinkles.co dokku "mongo:export wekan > $TARFILE &> /dev/null"&& 
echo "Success!"

# Download backup
echo "Downloading daily backup..."
scp root@uptwinkles.co:$TARFILE ../daily

if [ $(date +%u) -eq 7 ] ; then
	echo "Downloading weekly backup..."
	scp root@uptwinkles.co:$TARFILE ../weekly
fi

if [ $(date +%d) -eq 01 ] ; then
	echo "Downloading monthly backup..."
	scp root@uptwinkles.co:$TARFILE ../monthly
fi

# Delete original file from server
echo "Removing original file..."
ssh root@uptwinkles.co rm -f $TARFILE
echo "Backup Complete!"
