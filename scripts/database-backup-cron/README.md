# Database Backup

This script is ran via a daily cron job that creates and pulls 
a compressed backup of our Wekan database. The script creates 
daily, weekly and monthly backup (depending on the date) when 
executed.
